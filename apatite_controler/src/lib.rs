use std::{collections::HashMap, hash::Hash};

use smallvec::{smallvec, SmallVec};

use winit::{
    dpi::PhysicalPosition,
    event::{AxisId, ElementState, KeyboardInput, MouseScrollDelta, Touch},
};

use legion::{system, systems::Builder, Resources};

use apatite_bundle::Bundle;

pub type KeyCode = winit::event::VirtualKeyCode;
pub type MouseButton = winit::event::MouseButton;
pub type ActionState = KeyState;

pub trait ActionName: Eq + Hash + Clone + Send + Sync + 'static {}
impl<K: Eq + Hash + Clone + Send + Sync + 'static> ActionName for K {}

#[derive(Debug, Default)]
pub struct Controler<K: ActionName> {
    actions: HashMap<K, Action>,
}

/// Creation and reading state
impl<K: ActionName> Controler<K> {
    pub fn new(actions: &[ControlerAction<K>]) -> Self {
        Self {
            actions: actions.iter().fold(HashMap::new(), |mut acc, entry| {
                acc.insert(
                    entry.name.clone(),
                    Action {
                        triggers: entry.triggers.iter().cloned().collect::<SmallVec<_>>(),
                        state: Default::default(),
                    },
                );
                acc
            }),
        }
    }

    pub fn state(&self, action: &K) -> ActionState {
        self.actions
            .get(action)
            .map(|a| a.state)
            .unwrap_or_default()
    }

    pub fn pressed(&self, action: &K) -> bool {
        self.state(action) == ActionState::Pressed || self.state(action) == ActionState::JustPressed
    }

    pub fn just_pressed(&self, action: &K) -> bool {
        self.state(action) == ActionState::JustPressed
    }

    pub fn released(&self, action: &K) -> bool {
        self.state(action) == ActionState::Released
            || self.state(action) == ActionState::JustReleased
    }

    pub fn just_released(&self, action: &K) -> bool {
        self.state(action) == ActionState::JustReleased
    }
}

/// Manipulation / synchronization with RawControler
impl<K: ActionName> Controler<K> {
    pub fn sync(&mut self, raw_controler: &RawControler) {
        for (_, action) in self.actions.iter_mut() {
            // TODO: entry uses type K, so every time an action is updated
            // a String is created. Maybe there's a way to avoid it?
            action.state = Self::eval_action(&action.triggers, raw_controler);
        }
    }

    fn eval_action(triggers: &[ActionTrigger], raw_controler: &RawControler) -> ActionState {
        let mut pressed = false;
        let mut just_pressed = false;
        let mut released = false;
        let mut just_released = false;
        for trigger in triggers {
            match trigger.eval(raw_controler) {
                ActionState::Pressed => pressed = true,
                ActionState::JustPressed => just_pressed = true,
                ActionState::Released => released = true,
                ActionState::JustReleased => just_released = true,
            }
        }
        if pressed || (just_pressed && just_released) {
            ActionState::Pressed
        } else if just_pressed {
            ActionState::JustPressed
        } else if just_released && !released {
            ActionState::JustReleased
        } else {
            ActionState::Released
        }
    }
}

pub struct ControlerAction<'a, K: ActionName> {
    pub name: K,
    pub triggers: &'a [ActionTrigger],
}

#[derive(Debug)]
struct Action {
    pub triggers: SmallVec<[ActionTrigger; 4]>,
    pub state: ActionState,
}

#[derive(Debug, Clone)]
pub enum ActionTrigger {
    Mouse(MouseButton),
    Key(KeyCode),
    Combo(Box<SmallVec<[ActionTrigger; 4]>>),
}

impl ActionTrigger {
    pub fn mouse(button: MouseButton) -> Self {
        Self::Mouse(button)
    }

    pub fn key(key: KeyCode) -> Self {
        Self::Key(key)
    }

    pub fn combo2(fst: Self, snd: Self) -> Self {
        Self::Combo(Box::new(smallvec![fst, snd]))
    }

    pub fn combo3(fst: Self, snd: Self, trd: Self) -> Self {
        Self::Combo(Box::new(smallvec![fst, snd, trd]))
    }

    pub fn combo4(fst: Self, snd: Self, trd: Self, frt: Self) -> Self {
        Self::Combo(Box::new(smallvec![fst, snd, trd, frt]))
    }

    pub fn eval(&self, raw_controler: &RawControler) -> ActionState {
        match self {
            Self::Mouse(button) => raw_controler.mouse_button_state(*button),
            Self::Key(keycode) => raw_controler.key_state(*keycode),
            Self::Combo(triggers) => Self::eval_combo(triggers, raw_controler),
        }
    }

    fn eval_combo(triggers: &[ActionTrigger], raw_controler: &RawControler) -> ActionState {
        if triggers.len() == 0 {
            ActionState::Released
        } else if triggers.len() == 1 {
            triggers[0].eval(raw_controler)
        } else {
            let mut pressed = false;
            let mut just_pressed = false;
            let mut released = false;
            let mut just_released = false;
            for trigger in triggers {
                match trigger.eval(raw_controler) {
                    ActionState::Pressed => pressed = true,
                    ActionState::JustPressed => just_pressed = true,
                    ActionState::Released => released = true,
                    ActionState::JustReleased => just_released = true,
                }
            }
            if pressed && !(just_pressed || released || just_released) {
                ActionState::Pressed
            } else if just_pressed && !(released || just_released) {
                ActionState::JustPressed
            } else if just_released && !(just_pressed || released) {
                ActionState::JustReleased
            } else {
                ActionState::Released
            }
        }
    }
}

#[derive(Debug)]
pub struct RawControler {
    pub focused: bool,
    pub mouse_position: (f64, f64),
    pub mouse_delta: (f64, f64),
    pub scroll_delta: (f64, f64),
    pub mouse_button_states: HashMap<MouseButton, KeyState>,
    pub keyboard_states: HashMap<KeyCode, KeyState>,
    pub axis_states: HashMap<AxisId, AxisState>,
}

/// Creation and reading state
impl RawControler {
    #[cfg(not(target_family = "macos"))]
    const KEY_INPUT_SOURCE: KeyInputSource = KeyInputSource::DeviceEvents;
    #[cfg(target_family = "macos")]
    const KEY_INPUT_SOURCE: KeyInputSource = KeyInputSource::WindowEvents;

    pub fn new() -> Self {
        Self {
            focused: true,
            mouse_position: (0.0, 0.0),
            mouse_delta: (0.0, 0.0),
            scroll_delta: (0.0, 0.0),
            mouse_button_states: HashMap::new(),
            keyboard_states: HashMap::new(),
            axis_states: HashMap::new(),
        }
    }

    pub fn key_state(&self, keycode: KeyCode) -> KeyState {
        self.keyboard_states
            .get(&keycode)
            .cloned()
            .unwrap_or_default()
    }

    pub fn mouse_button_state(&self, button: MouseButton) -> KeyState {
        self.mouse_button_states
            .get(&button)
            .cloned()
            .unwrap_or_default()
    }

    pub fn axis_state(&self, axis: AxisId) -> AxisState {
        self.axis_states.get(&axis).map(|s| *s).unwrap_or_default()
    }
}

/// Inner state management
impl RawControler {
    pub fn new_cycle(&mut self) {
        self.mouse_delta = (0.0, 0.0);
        self.scroll_delta = (0.0, 0.0);
        for (_, state) in self.mouse_button_states.iter_mut() {
            match state {
                KeyState::JustPressed => *state = KeyState::Pressed,
                KeyState::JustReleased => *state = KeyState::Released,
                _ => (),
            }
        }
        for (_, state) in self.keyboard_states.iter_mut() {
            match state {
                KeyState::JustPressed => *state = KeyState::Pressed,
                KeyState::JustReleased => *state = KeyState::Released,
                _ => (),
            }
        }
        // TODO: Is this necesary or breaking?
        // for (_, axis_state) in self.axis_states.iter_mut() {
        //     axis_state.value = 0.0;
        // }
    }

    pub fn release_all(&mut self) {
        for (_, state) in self.mouse_button_states.iter_mut() {
            *state = KeyState::Released;
        }
        for (_, state) in self.keyboard_states.iter_mut() {
            *state = KeyState::Released;
        }
        for (_, state) in self.axis_states.iter_mut() {
            state.value = 0.0;
        }
    }

    fn keyboard_input(&mut self, input: KeyboardInput) {
        if let Some(keycode) = input.virtual_keycode {
            let key_state = self.keyboard_states.entry(keycode).or_default();
            match input.state {
                ElementState::Pressed => {
                    if *key_state == KeyState::Released || *key_state == KeyState::JustReleased {
                        *key_state = KeyState::JustPressed;
                    } else {
                        *key_state = KeyState::Pressed;
                    }
                }
                ElementState::Released => {
                    if *key_state == KeyState::Pressed || *key_state == KeyState::JustPressed {
                        *key_state = KeyState::JustReleased;
                    } else {
                        *key_state = KeyState::Released;
                    }
                }
            }
        }
    }
}

/// Window events
impl RawControler {
    pub fn focus_change(&mut self, focused: bool) {
        self.focused = focused;
        if !self.focused {
            self.release_all();
        }
    }

    pub fn keyboard_input_window(&mut self, input: KeyboardInput) {
        if let Self::KEY_INPUT_SOURCE = KeyInputSource::WindowEvents {
            if self.focused {
                self.keyboard_input(input);
            }
        }
    }

    pub fn cursor_moved(&mut self, position: PhysicalPosition<f64>) {
        self.mouse_position = (position.x, position.y);
    }

    pub fn mouse_scroll(&mut self, delta: MouseScrollDelta) {
        match delta {
            MouseScrollDelta::LineDelta(x, y) => {
                self.scroll_delta = (x as f64, y as f64);
            }
            MouseScrollDelta::PixelDelta(delta) => {
                self.scroll_delta = (delta.x, delta.y);
            }
        }
    }

    pub fn mouse_input(&mut self, button: MouseButton, state: ElementState) {
        let key_state = self.mouse_button_states.entry(button).or_default();
        match state {
            ElementState::Pressed => {
                if *key_state == KeyState::Released || *key_state == KeyState::JustReleased {
                    *key_state = KeyState::JustPressed;
                } else {
                    *key_state = KeyState::Pressed;
                }
            }
            ElementState::Released => {
                if *key_state == KeyState::Pressed || *key_state == KeyState::JustPressed {
                    *key_state = KeyState::JustReleased;
                } else {
                    *key_state = KeyState::Released;
                }
            }
        }
    }

    pub fn axis_motion(&mut self, axis: AxisId, value: f64) {
        *self.axis_states.entry(axis).or_default() = AxisState { value };
    }

    pub fn touch(&mut self, _touch: Touch) {
        println!("RawControler touch handling not implemented!");
    }
}

/// Device events
impl RawControler {
    pub fn mouse_moved(&mut self, delta: (f64, f64)) {
        self.mouse_delta = delta;
    }

    pub fn keyboard_input_device(&mut self, input: KeyboardInput) {
        if let Self::KEY_INPUT_SOURCE = KeyInputSource::DeviceEvents {
            if self.focused {
                self.keyboard_input(input);
            }
        }
    }
}

impl Default for RawControler {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum KeyInputSource {
    WindowEvents,
    DeviceEvents,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum KeyState {
    Released,
    JustPressed,
    Pressed,
    JustReleased,
}

impl Default for KeyState {
    fn default() -> Self {
        KeyState::Released
    }
}

#[derive(Debug, PartialEq, Copy, Clone, Default)]
pub struct AxisState {
    pub value: f64,
}

#[system]
fn controler_sync<K: 'static + ActionName>(
    #[resource] controler: &mut Controler<K>,
    #[resource] raw_controler: &RawControler,
) {
    controler.sync(raw_controler);
}

#[system]
fn raw_controler_cycle(#[resource] raw_controler: &mut RawControler) {
    raw_controler.new_cycle()
}

pub struct ControlerBundle<K: ActionName> {
    controler: Controler<K>,
}

impl<K: ActionName> ControlerBundle<K> {
    pub fn new(controler: Controler<K>) -> Self {
        Self { controler }
    }
}

impl<K: ActionName> Bundle for ControlerBundle<K> {
    fn insert(
        self,
        resources: &mut Resources,
        _early_subcycle_systems: &mut Builder,
        early_cycle_systems: &mut Builder,
        late_cycle_systems: &mut Builder,
        _late_subcycle_systems: &mut Builder,
    ) {
        resources.insert(RawControler::new());
        resources.insert(self.controler);
        early_cycle_systems.add_system(controler_sync_system::<K>());
        late_cycle_systems.add_system(raw_controler_cycle_system());
    }
}

impl<K: ActionName> From<Controler<K>> for ControlerBundle<K> {
    fn from(controler: Controler<K>) -> Self {
        Self::new(controler)
    }
}
