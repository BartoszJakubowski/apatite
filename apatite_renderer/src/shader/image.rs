pub const DEFAULT_VERTEX: &str = "
#version 450

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec2 fragTexCoord;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
    fragTexCoord = uv;
}
";

pub const DEFAULT_FRAGMENT: &str = "
#version 450

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform texture2D diffuse_texture;
layout(set = 0, binding = 1) uniform sampler diffuse_sampler;

void main() {
    f_color = texture(sampler2D(diffuse_texture, diffuse_sampler), fragTexCoord);
}
";
