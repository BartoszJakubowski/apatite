//! Wallpaper object

use wgpu::{BindGroup, Buffer, RenderPipeline};

pub mod image;

pub trait Object: Send + Sync + 'static {
    fn pipeline(&self) -> &RenderPipeline;

    fn buffer(&self) -> &Buffer;

    fn buffer_len(&self) -> u32;

    fn bind_group(&self) -> Option<&BindGroup>;
}
