use std::sync::Arc;
use wgpu::{AddressMode, Device, FilterMode, Sampler, SamplerDescriptor};

#[derive(Debug, Clone)]
pub struct DefaultSamplers {
    pub image: Arc<Sampler>,
}

impl DefaultSamplers {
    pub fn new(device: &Device) -> Self {
        Self {
            image: Arc::new(default_image_sampler(device)),
        }
    }
}

fn default_image_sampler(device: &Device) -> Sampler {
    device.create_sampler(&SamplerDescriptor {
        label: Some("default image sampler"),
        address_mode_u: AddressMode::ClampToEdge,
        address_mode_v: AddressMode::ClampToEdge,
        address_mode_w: AddressMode::ClampToEdge,
        mag_filter: FilterMode::Linear,
        min_filter: FilterMode::Linear,
        anisotropy_clamp: None,
        mipmap_filter: FilterMode::Nearest,
        lod_max_clamp: 100.0,
        lod_min_clamp: -100.0,
        compare: None,
    })
}
