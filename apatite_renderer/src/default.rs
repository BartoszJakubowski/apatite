use std::error::Error;
use wgpu::Device;

use super::bind_group::DefaultBindGroupLayouts;
use super::pipeline::DefaultPipelines;
use super::sampler::DefaultSamplers;
use super::shader::DefaultShaders;

#[derive(Debug, Clone)]
pub struct RenderingDefaults {
    pub shaders: DefaultShaders,
    pub pipelines: DefaultPipelines,
    pub bind_group_layouts: DefaultBindGroupLayouts,
    pub samplers: DefaultSamplers,
}

impl RenderingDefaults {
    pub fn new(device: &Device) -> Result<Self, Box<dyn Error>> {
        let shaders = DefaultShaders::new(device)?;
        let bind_group_layouts = DefaultBindGroupLayouts::new(device);
        let samplers = DefaultSamplers::new(device);
        let pipelines = DefaultPipelines::new(device, &bind_group_layouts, &shaders);
        Ok(Self {
            shaders,
            pipelines,
            bind_group_layouts,
            samplers,
        })
    }
}
