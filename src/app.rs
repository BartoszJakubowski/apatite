use apatite_bundle::Bundle;
use apatite_controler::{ActionName, ControlerBundle, RawControler};
use apatite_renderer::{context::RendererContext, Renderer};
use apatite_timer::{Timer, TimerBundle};
use legion::{
    systems::{Builder, Resource},
    Resources, Schedule, World,
};
use std::error::Error;
use winit::{
    event::{DeviceEvent, Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{WindowAttributes, WindowBuilder},
};

pub struct AppSettings<K: ActionName> {
    pub window_attributes: Option<WindowAttributes>,
    pub timer: Timer,
    pub fps_limiter: (),
    pub controller_bundle: ControlerBundle<K>,
    pub systems: Schedule,
}

pub struct App {
    resources: Resources,
    world: World,
    early_cycle_systems_builder: Builder,
    late_cycle_systems_builder: Builder,
    early_subcycle_systems_builder: Builder,
    late_subcycle_systems_builder: Builder,
}

/// Creating new app
impl App {
    pub fn new() -> Self {
        Self {
            resources: Default::default(),
            world: Default::default(),
            early_cycle_systems_builder: Schedule::builder(),
            late_cycle_systems_builder: Schedule::builder(),
            early_subcycle_systems_builder: Schedule::builder(),
            late_subcycle_systems_builder: Schedule::builder(),
        }
    }
}

/// Winit event handling
impl App {
    fn window_event(&mut self, event: WindowEvent) {
        match event {
            WindowEvent::CloseRequested => {
                panic!("WindowEvent::CloseRequested should be handled before sending event to App!")
            }
            WindowEvent::Resized(size) => self.mut_resource(|r: &mut Renderer| r.resize(size)),
            WindowEvent::Focused(focused) => {
                self.mut_resource(|rc: &mut RawControler| rc.focus_change(focused))
            }
            WindowEvent::KeyboardInput { input, .. } => {
                self.mut_resource(|rc: &mut RawControler| rc.keyboard_input_window(input))
            }
            WindowEvent::CursorMoved { position, .. } => {
                self.mut_resource(|rc: &mut RawControler| rc.cursor_moved(position))
            }
            WindowEvent::MouseWheel { delta, .. } => {
                self.mut_resource(|rc: &mut RawControler| rc.mouse_scroll(delta))
            }
            WindowEvent::MouseInput { button, state, .. } => {
                self.mut_resource(|rc: &mut RawControler| rc.mouse_input(button, state))
            }
            WindowEvent::AxisMotion { axis, value, .. } => {
                self.mut_resource(|rc: &mut RawControler| rc.axis_motion(axis, value))
            }
            WindowEvent::Touch(touch) => self.mut_resource(|rc: &mut RawControler| rc.touch(touch)),
            _ => (),
        }
    }

    fn device_event(&mut self, event: DeviceEvent) {
        match event {
            DeviceEvent::MouseMotion { delta } => {
                self.mut_resource(|rc: &mut RawControler| rc.mouse_moved(delta))
            }
            DeviceEvent::Key(keyboard_input) => {
                self.mut_resource(|rc: &mut RawControler| rc.keyboard_input_device(keyboard_input))
            }
            _ => (),
        }
    }
}

/// Convinience functions
impl App {
    /// Convinient mutation of resources
    /// Panics if resource isn't present
    fn mut_resource<T: Resource, F: FnOnce(&mut T)>(&mut self, fun: F) {
        if let Some(mut resource) = self.resources.get_mut::<T>() {
            fun(&mut resource);
        } else {
            log::error!(
                "App::mut_resource(): {} resource not in store!",
                std::any::type_name::<T>()
            )
        }
    }

    fn insert_bundle(&mut self, bundle: impl Bundle) {
        bundle.insert(
            &mut self.resources,
            &mut self.early_subcycle_systems_builder,
            &mut self.early_cycle_systems_builder,
            &mut self.late_cycle_systems_builder,
            &mut self.late_subcycle_systems_builder,
        )
    }

    fn build_schedules(&mut self) -> (Schedule, Schedule, Schedule, Schedule) {
        (
            self.early_subcycle_systems_builder.build(),
            self.early_cycle_systems_builder.build(),
            self.late_cycle_systems_builder.build(),
            self.late_subcycle_systems_builder.build(),
        )
    }
}

/// Running
impl App {
    pub fn run<K: ActionName>(mut self, settings: AppSettings<K>) -> Result<(), Box<dyn Error>> {
        // Create event loop
        let event_loop = EventLoop::new();

        // Create window if WindowAttributes exist.
        let window = if let Some(window_attributes) = settings.window_attributes {
            let mut window_builder = WindowBuilder::new();
            window_builder.window = window_attributes;
            Some(window_builder.build(&event_loop)?)
        } else {
            None
        };

        // Create renderer context and renderer if window exists.
        let (renderer_context, renderer) = if let Some(window) = window {
            let context = RendererContext::new(window)?;
            let renderer = Renderer::new(context.clone())?;
            (Some(context), Some(renderer))
        } else {
            (None, None)
        };

        // Temp renderer test
        if let Some(renderer) = &renderer {
            self.late_subcycle_systems_builder
                .add_system(renderer_objects_system());
            self.world.push((super::RendererObject(Box::new(
                apatite_renderer::object::image::Image::new_default(
                    renderer.context().device(),
                    renderer.defaults(),
                    &apatite_renderer::object::image::load_texture(
                        renderer.context().device(),
                        renderer.context().queue(),
                        &"test.png",
                    )?,
                ),
            )),));
        }

        // Add required bundles
        self.insert_bundle(settings.controller_bundle);
        self.insert_bundle(TimerBundle::new(settings.timer));
        // TODO: create RendererBundle
        // if let Some(window) = window {
        //     self.insert_bundle(RendererBundle::new(window));
        // }

        // Insert created resources.
        if let Some(renderer) = renderer {
            self.resources.insert(renderer_context.unwrap());
            self.resources.insert(renderer);
        }

        // Build schedules
        let (
            mut early_subcycle_systems,
            mut early_cycle_systems,
            mut late_cycle_systems,
            mut late_subcycle_systems,
        ) = self.build_schedules();

        // Take user systems schedule
        let mut user_systems = settings.systems;

        event_loop.run(move |event, _, control_flow| {
            match event {
                // Begin a new loop iteration.
                Event::NewEvents(_start_cause) => {}

                // Process events.
                Event::WindowEvent { event, .. } => {
                    if let WindowEvent::CloseRequested = event {
                        *control_flow = ControlFlow::Exit;
                    } else {
                        self.window_event(event);
                    }
                }
                Event::DeviceEvent { event, .. } => self.device_event(event),
                Event::UserEvent(_) => (),
                Event::Suspended => (),
                Event::Resumed => (),

                // Render the next frame.
                Event::MainEventsCleared => {
                    early_subcycle_systems.execute(&mut self.world, &mut self.resources);
                    early_cycle_systems.execute(&mut self.world, &mut self.resources);
                    user_systems.execute(&mut self.world, &mut self.resources);
                    late_cycle_systems.execute(&mut self.world, &mut self.resources);
                    late_subcycle_systems.execute(&mut self.world, &mut self.resources);
                }

                Event::RedrawRequested(_) => (),
                Event::RedrawEventsCleared => {
                    // TODO implement fps limiter
                    std::thread::sleep_ms((1000.0 / 60.0) as u32);
                }

                // Shut down the program.
                Event::LoopDestroyed => {}
            };
        });
    }
}

// Temp renderer test
#[legion::system()]
#[read_component(super::RendererObject)]
fn renderer_objects(#[resource] renderer: &mut Renderer, world: &legion::world::SubWorld) {
    use legion::IntoQuery;
    let mut query = <&super::RendererObject>::query();
    let objects = query.iter(world).map(|o| o.0.as_ref()).collect::<Vec<_>>();

    if let Err(_) = renderer.draw_frame(objects.as_slice()) {
        panic!("Renderer fucked up");
    }
}
