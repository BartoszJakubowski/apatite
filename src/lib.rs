pub mod app;

pub use app::{App, AppSettings};

pub struct RendererObject(Box<dyn apatite_renderer::object::Object>);
