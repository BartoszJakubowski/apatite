use legion::{systems::Builder, Resources};

pub trait Bundle {
    fn insert(
        self,
        resources: &mut Resources,
        early_subcycle_systems: &mut Builder,
        early_cycle_systems: &mut Builder,
        late_cycle_systems: &mut Builder,
        late_subcycle_systems: &mut Builder,
    );
}
